#![feature(core_intrinsics, core_panic_info, lang_items, global_asm)]
// quickcheck doesn't yet work with #[no_std]!
//#![cfg_attr(test, feature(plugin))]
//#![cfg_attr(test, plugin(quickcheck_macros))]
#![no_std]

// FIXME: support more than just x86_64!
mod embedded_hal_ext;
mod start;
mod utest_handler;
use self::embedded_hal_ext::{devices, IOPort, IRQ};

impl IOPort<u8> for (u16, sel4::X86IOPort) {
    type Error = sel4::Error;

    fn read(&mut self, offset: u16) -> nb::Result<u8, Self::Error> {
        self.1.read8(self.0 + offset).map_err(nb::Error::Other)
    }

    fn write(&mut self, offset: u16, word: u8) -> nb::Result<(), Self::Error> {
        self.1
            .write8((self.0 + offset) as usize, word as usize)
            .map_err(nb::Error::Other)
    }
}

// FIXME: support sharing notifications using badges
impl IRQ for (sel4::Notification, sel4::IRQHandler) {
    type Error = sel4::Error;

    fn poll(&mut self) -> nb::Result<(), Self::Error> {
        match self.0.poll() {
            Ok(0) => Err(nb::Error::WouldBlock),
            // check badge and stash the rest to wake other tasks that were blocking on the same
            // notification
            Ok(_) => Ok(()),
            Err(e) => Err(nb::Error::Other(e)),
        }
    }

    fn acknowledge(&mut self) -> nb::Result<(), Self::Error> {
        self.1.acknowledge().map_err(nb::Error::Other)
    }
}

fn main() {
    let sel4::bootinfo::RootCaps {
        ioport_control,
        irq_control,
        init_thread_cnode,
        // bootinfo,
        ..
    } = sel4::bootinfo::take_root_caps();
    /*
    let ntfn = {
      //// FIXME: CPtr allocation is more complicated than this...
      //let ntfn = init_thread_cnode.mint::<Notification>(..).unwrap();
      //// FIXME: Untyped allocation is more complicated than this?
      //_untyped_allocator.allocate(ntfn).unwrap();
      //ntfn
    };
    let irq_handler = {
      //// FIXME: CPtr allocation is more complicated than this...
      //let irq_handler = init_thread_cnode.mint::<IRQHandler>(..).unwrap();
      //irq_control
      //    .get_ioapic(irq_handler, 0, 4, false, false, 0)
      //    .unwrap();
      //irq_handler
    };
    let com1 = devices::Serial::new(
        (0x3f8, ioport_control.issue(0x3f8..0x3f8 + 7).unwrap()),
        (ntfn, irq_handler),
    );
    writeln!(com1, "Hello, world!");
    let (reader, writer) = com1.split();
    for c in reader {
        // FIXME: read c into a [u8;4] -> char (UTF-8); if invalid UTF-8 print the hex
        // representation.
        writeln!(writer, "Received character: {}", c);
    }
    */
}
