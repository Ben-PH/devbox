// build.rs will add debug-related syscalls to these struct

pub struct Debug(pub(crate) ());

pub struct DebugPrinter(pub(crate) ());

#[cfg(feature = "CONFIG_PRINTING")]
impl core::fmt::Write for DebugPrinter {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for b in s.as_bytes() {
            unsafe { self.put_char(*b as i8) };
        }
        Ok(())
    }
}

#[cfg(not(feature = "CONFIG_PRINTING"))]
impl core::fmt::Write for DebugPrinter {
    fn write_str(&mut self, _: &str) -> core::fmt::Result {
        Err(core::fmt::Error)
    }
}
