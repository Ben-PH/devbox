/// An unowned capability pointer. Use `Cap` for owned handles.
#[derive(Clone, Copy)]
pub struct CPtr(pub(crate) usize);

impl CPtr {
    #[inline(always)]
    pub unsafe fn from_usize(usz: usize) -> Self {
        CPtr(usz)
    }

    #[inline(always)]
    pub fn as_usize(&self) -> usize {
        self.0
    }
}

/// An owned CPtr. Unsafe to construct and unsafe to invoke.
pub struct Cap(CPtr);

impl Cap {
    #[inline(always)]
    pub unsafe fn from_cptr(cptr: CPtr) -> Self {
        Cap(cptr)
    }

    #[inline(always)]
    pub fn as_cptr(&self) -> CPtr {
        self.0
    }

    // Syscall stubs that have a seL4_CPtr as the first argument are added as member functions to
    // this struct in build.rs
}
